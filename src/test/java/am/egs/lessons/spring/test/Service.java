package am.egs.lessons.spring.test;

/**
 * Created by User on 5/25/2019.
 */

import am.egs.lessons.spring.config.AppConfig;
import am.egs.lessons.spring.entity.Person;
import org.junit.Test;
import am.egs.lessons.spring.service.PersonService;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;


public class Service {

    @Test
    public void storeTest() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);

        PersonService personService = context.getBean(PersonService.class);
        // Add Persons
        personService.add(new Person("Rahul", "Gupta", "rahulgupta@company.com"));
        personService.add(new Person("Akshay", "Sharma", "akshaysharma@company.com"));
        personService.add(new Person("Ankit", "Sarraf", "ankitsarraf@company.com"));

        List<Person> persons = personService.listPersons();
        for (Person person : persons) {
            System.out.println("Id = "+person.getId());
            System.out.println("First Name = "+person.getFirstName());
            System.out.println("Last Name = "+person.getLastName());
            System.out.println("Email = "+person.getEmail());
            System.out.println();
        }

        context.close();

    }

}
